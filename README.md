Działająca aplikacja:  http://intense-oasis-3420.herokuapp.com/news/

Wprowadzone funkcjonalnosci:
Pajak sieciowy indeksujacy newsy z portali uruchamiany "recznie" - 1pkt.
Pelna wyszukiwarka z operatorami - 3 pkt.
Mechanizm cache'owania zapytan - 1 pkt.
Prawidlowe odswiezanie cache'a (po zejsciu tematu z wokandy) - 1 pkt.
Wszystkie komponenty aplikacji dzialaja w chmurze - 1 pkt
Aplikacja korzysta z bazy produkcyjnej (nie SQLite) - 1 pkt
Suma 8pkt.
