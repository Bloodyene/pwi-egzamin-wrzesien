# -*- coding: utf-8 -*-
from datetime import datetime, date, time
import requests
import html5lib
from bs4 import BeautifulSoup


def scrap_webpage_onet(url, news_class):
    news_result = []
    session = requests.session()
    result = session.get(url)

    source = result.text
    soup = BeautifulSoup(source, 'html5lib')

    news =soup.find_all('span', {'class': str(news_class)})

    for n in news:
        news_result.append(convert_to_string_without_polish_chars(n.string))

    return news_result


def scrap_webpage_interia(url, news_class):
    news_result = []
    session = requests.session()
    result = session.get(url)

    source = result.text
    soup = BeautifulSoup(source, 'html5lib')

    news =soup.find_all('a', {'class': str(news_class)})

    for n in news:
        news_result.append(convert_to_string_without_polish_chars(n.string))

    return news_result


def scrap_webpage_wp(url):
    news_result = []
    session = requests.session()
    result = session.get(url)

    source = result.text
    soup = BeautifulSoup(source, 'html5lib')

    news =soup.find_all('h3')

    for n in news:
        news_result.append(convert_to_string_without_polish_chars(n.string))

    return news_result


def convert_to_string_without_polish_chars(element):
    if element is not None and element is not None:
        return ("".join([x if ord(x) < 128 else '?' for x in element])).strip()
