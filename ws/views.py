from django.shortcuts import render , redirect
import urllib2
from models import  *
from forms import *
import re
import html5lib
import BeautifulSoup
Soup = BeautifulSoup.BeautifulSoup
import requests
import crawl
import memcache
import redis
from rq.decorators import job
from memcache import RedisCache


conn = redis.StrictRedis(host='pub-redis-16716.us-east-1-2.1.ec2.garantiadata.com', port=16716, db=0, password="Trololo1")
mc = RedisCache(conn)

# Create your views here.
def index(request):
    return render(request, 'index.html')




def onet(request):
    context = None
    onet_cache = mc.get('onet-cache')

    if onet_cache is None:
        Onett.objects.all().delete()
        url2 = "http://www.onet.pl"
        tabela_danych2 = crawl.scrap_webpage_onet(url2,'newsTitle')

        for j in tabela_danych2:
            desc = Onett(description=j)
            desc.save()

        string = ''
        for i in tabela_danych2:
            string += i + '|'

        mc.set('onet-cache', string, 60)

        context = {'baza': tabela_danych2}
    else:
        onet_cache_array = onet_cache.split('|')
        context = {'baza': onet_cache_array}

    return render(request, 'onet.html', context)

def interia(request):

    Interiaa.objects.all().delete()
    baza = crawl.scrap_webpage_interia('http://www.interia.pl','news-a')
    for j in baza:
        desc = Interiaa(description=j)
        desc.save()

    context = {'baza': baza}
    return render(request, 'interia.html', context)

def wp(request):
    Wpp.objects.all().delete()
    site_baza = crawl.scrap_webpage_wp('http://www.wp.pl')

    for j in site_baza:
        if j is not None:
            desc = Wpp(description=j)
            desc.save()

    context = {'baza': site_baza}
    return render(request, 'wp.html', context)

def wyszukane(request):
    zapytanie = request.session.get('lista')
    baza = Wyszukiwarkaa.objects.all()

    print('LICZBA NEWSOW: ' + str(len(baza)))
    title = ''
    if baza.count() == 0:
        title = 'Nie ma zadnego newsa ...'
    else:
        title = 'Wyszukane newsy:'
    context = {'baza' : baza , 'title' : title , 'zapytanie' : zapytanie}

    return render(request, 'wyszukane.html', context)

def pobieranieNewsow():
    news = []

    onet_news = Onett.objects.all()
    interia_news = Interiaa.objects.all()
    wp_news = Wpp.objects.all()

    if onet_news is not None:
        for o in onet_news:
            news.append(o.description)

    if interia_news is not None:
        for o in interia_news:
            news.append(o.description)

    if wp_news is not None:
        for o in wp_news:
            news.append(o.description)

    return news

def szukaj(request):
    form = ComForm(request.POST)
    if form.is_valid():

        tak =0

        listaSlow = []

        Wyszukiwarkaa.objects.all().delete()

        slowo = form.cleaned_data.get('slowo')

        lista_wynikow_do_cache = []

        # JESLI W CACHE


        if mc.get(str(slowo)) is not None:
            naglowki_z_cache = mc.get(str(slowo))
            cache_array = naglowki_z_cache.split('|')
            for naglowek in cache_array:
                z = Wyszukiwarkaa(description=naglowek)
                z.save()

            return redirect('wyszukane')


        lista_slow_wersja_plus = slowo.split(' ')
        listaSlow = slowo.split()


        if ' ' in slowo:
            tak = 1
        else:
            tak = 0

        list = []
        list = pobieranieNewsow()

        for i in lista_slow_wersja_plus:
            print(i)
            if str(i[0]) == '+':
                tak = 2

        for i in lista_slow_wersja_plus:
            print(i)
            if str(i[0]) == '-':
                tak = 3

        # if listaSlow[0][0] == '+':
        #     tak = 2
        if listaSlow[0][0] == '-':
            tak = 3
        if listaSlow[0][0] == '"':
            tak = 5


        g = 0

        #wyszukuje po wskazanych slowach oddzielonych spacja tylko dla ktoregos ze wskazanych slow
        if tak == 1 or tak == 0:
            for k in list:
                kk = []
                kk = re.sub('[^A-Za-z0-9]+', ' ', k).split()
                for j in kk:
                    for h in listaSlow:
                        if h.lower() == j.lower():
                            lista_wynikow_do_cache.append(k)
                            z = Wyszukiwarkaa(description = k)
                            z.save()




        if tak == 2:
            # te z plusem
            wymagane = []

            # te bez plusa
            wybrane_naglowki = []

            count = 0
            for s in lista_slow_wersja_plus:
                print(s[0])
                if s[0] == '+':
                    slowo_do_dodania = s[1:]
                    print(slowo_do_dodania)
                    wymagane.append(slowo_do_dodania)
                    lista_slow_wersja_plus.pop(count)
                count = count + 1


            for i in lista_slow_wersja_plus:
                print(i)


            for slowo_nie_wymagane in lista_slow_wersja_plus:
                for naglowek in list:
                    if slowo_nie_wymagane in naglowek:
                        print(naglowek)
                        wybrane_naglowki.append(naglowek)

            if len(lista_slow_wersja_plus) == 0:
                wybrane_naglowki = list


            for slowo_wymagane in wymagane:
                print('# '+slowo_wymagane)
                for naglowek in wybrane_naglowki:
                    if slowo_wymagane in naglowek:
                        print(naglowek)
                        lista_wynikow_do_cache.append(naglowek)
                        z = Wyszukiwarkaa(description=naglowek)
                        z.save()







        if tak == 3:
             # te z plusem
            wymagane = []

            # te bez plusa
            wybrane_naglowki = []

            count = 0
            for s in lista_slow_wersja_plus:

                if s[0] == '-':
                    slowo_do_dodania = s[1:]

                    wymagane.append(slowo_do_dodania)
                    lista_slow_wersja_plus.pop(count)
                count = count + 1


            for i in lista_slow_wersja_plus:
                print(i)


            for slowo_nie_wymagane in lista_slow_wersja_plus:
                for naglowek in list:
                    if slowo_nie_wymagane in naglowek:
                        print(naglowek)
                        wybrane_naglowki.append(naglowek)



            if len(lista_slow_wersja_plus) == 0:
                wybrane_naglowki = list

            for slowo_wymagane in wymagane:

                for naglowek in wybrane_naglowki:
                    if slowo_wymagane in naglowek:
                        print(naglowek)
                    else:
                        lista_wynikow_do_cache.append(naglowek)
                        z = Wyszukiwarkaa(description=naglowek)
                        z.save()

        if tak == 5:
            length = len(slowo)
            wyraz_arr = slowo[1:length-1]
            wyraz = ''.join([str(x) for x in wyraz_arr])


            for i in list:
                if wyraz in i:
                    lista_wynikow_do_cache.append(i)
                    z = Wyszukiwarkaa(description=i)
                    z.save()

        request.session['lista'] = slowo

        string = ''
        for i in lista_wynikow_do_cache:
            string += i + '|'

        mc.set(slowo, string, 30)

        return redirect('wyszukane')


    return render(request,'wyszukaj.html', locals())
